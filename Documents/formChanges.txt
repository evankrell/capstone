
Before:

rails g scaffold EOCM document:references evaluation:string comment_outcomes:text comment_worked:text comment_changes:text comment_rec:text comment_implemented:text comment_results:text comment_additional:text
rails g scaffold AIRF document:references analysis:text taken:text conclusion:text
rails g scaffold PIE document:references description:text tpercent:float tscore:text rpercent:float rscore calculation:text


Proposed:

rails g scaffold EOCM document:references evaluation:string comment_outcomes:text comment_worked:text comment_changes:text comment_rec:text comment_implemented:text comment_results:text comment_additional:text \
	comment_failed:text

rails g scaffold AIRF document:references analysis:text taken:text conclusion:text \
	name:string, assignee1:user, assignee2:user, assignee3:user, assignee4:user, meetingName:string, meetingDate:date

rails g scaffold PIE document:references description:text tpercent:float tscore:text rpercent:float rscore calculation:text \
	name:text, ABETOutcome:string
