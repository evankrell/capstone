# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161128034837) do

  create_table "abet_metrics", force: :cascade do |t|
    t.integer  "offering_id", limit: 4
    t.string   "abet",        limit: 255
    t.string   "rating",      limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "abet_metrics", ["offering_id"], name: "index_abet_metrics_on_offering_id", using: :btree

  create_table "abet_outcomes", force: :cascade do |t|
    t.integer  "track_id",   limit: 4
    t.string   "code",       limit: 255
    t.text     "desc",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "abet_outcomes", ["track_id"], name: "index_abet_outcomes_on_track_id", using: :btree

  create_table "airs", force: :cascade do |t|
    t.integer  "document_id", limit: 4
    t.string   "name",        limit: 255
    t.string   "abetOutcome", limit: 255
    t.string   "assignee1",   limit: 255
    t.string   "assignee2",   limit: 255
    t.string   "assignee3",   limit: 255
    t.string   "assignee4",   limit: 255
    t.string   "meetingName", limit: 255
    t.datetime "meetingDate"
    t.text     "analysis",    limit: 65535
    t.text     "taken",       limit: 65535
    t.text     "conclusion",  limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "airs", ["document_id"], name: "index_airs_on_document_id", using: :btree

  create_table "approvers", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "track_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "approvers", ["track_id"], name: "index_approvers_on_track_id", using: :btree
  add_index "approvers", ["user_id"], name: "index_approvers_on_user_id", using: :btree

  create_table "authors", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "comment_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "document_id", limit: 4
    t.text     "text",        limit: 65535
    t.datetime "date"
    t.string   "type",        limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "course_outcomes", force: :cascade do |t|
    t.integer  "offering_id", limit: 4
    t.text     "outcome",     limit: 65535
    t.string   "rating",      limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "course_outcomes", ["offering_id"], name: "index_course_outcomes_on_offering_id", using: :btree

  create_table "course_track_lists", force: :cascade do |t|
    t.integer  "track_id",   limit: 4
    t.integer  "course_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "course_track_lists", ["course_id"], name: "index_course_track_lists_on_course_id", using: :btree
  add_index "course_track_lists", ["track_id"], name: "index_course_track_lists_on_track_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "code_suffix", limit: 255
    t.string   "code_prefix", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "degrees", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "status",     limit: 255
    t.datetime "assigned"
    t.datetime "due"
    t.string   "formType",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "eocms", force: :cascade do |t|
    t.integer  "document_id",         limit: 4
    t.string   "evaluation",          limit: 255
    t.text     "comment_outcomes",    limit: 65535
    t.text     "comment_worked",      limit: 65535
    t.text     "comment_changes",     limit: 65535
    t.text     "comment_rec",         limit: 65535
    t.text     "comment_implemented", limit: 65535
    t.text     "comment_results",     limit: 65535
    t.text     "comment_additional",  limit: 65535
    t.text     "comment_failed",      limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "eocms", ["document_id"], name: "index_eocms_on_document_id", using: :btree

  create_table "libraries", force: :cascade do |t|
    t.integer  "offering_id", limit: 4
    t.integer  "document_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "libraries", ["document_id"], name: "index_libraries_on_document_id", using: :btree
  add_index "libraries", ["offering_id"], name: "index_libraries_on_offering_id", using: :btree

  create_table "master_matrices", force: :cascade do |t|
    t.integer  "track_id",        limit: 4
    t.integer  "course_id",       limit: 4
    t.integer  "abet_outcome_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "master_matrices", ["abet_outcome_id"], name: "index_master_matrices_on_abet_outcome_id", using: :btree
  add_index "master_matrices", ["course_id"], name: "index_master_matrices_on_course_id", using: :btree
  add_index "master_matrices", ["track_id"], name: "index_master_matrices_on_track_id", using: :btree

  create_table "offerings", force: :cascade do |t|
    t.string   "CRN",        limit: 255
    t.string   "section",    limit: 255
    t.integer  "year",       limit: 4
    t.string   "term",       limit: 255
    t.string   "evaluation", limit: 255
    t.integer  "course_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "offerings", ["course_id"], name: "index_offerings_on_course_id", using: :btree

  create_table "pies", force: :cascade do |t|
    t.integer  "document_id",  limit: 4
    t.text     "name",         limit: 65535
    t.string   "abet_outcome", limit: 255
    t.text     "description",  limit: 65535
    t.float    "tpercent",     limit: 24
    t.text     "tscore",       limit: 65535
    t.float    "rpercent",     limit: 24
    t.string   "rscore",       limit: 255
    t.text     "calculation",  limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "pies", ["document_id"], name: "index_pies_on_document_id", using: :btree

  create_table "resources", force: :cascade do |t|
    t.integer  "document_id", limit: 4
    t.text     "location",    limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "teachings", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "offering_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "teachings", ["offering_id"], name: "index_teachings_on_offering_id", using: :btree
  add_index "teachings", ["user_id"], name: "index_teachings_on_user_id", using: :btree

  create_table "tracks", force: :cascade do |t|
    t.integer  "degree_id",  limit: 4
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "tracks", ["degree_id"], name: "index_tracks_on_degree_id", using: :btree

  create_table "uploaders", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "file_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "password",        limit: 255
    t.string   "fname",           limit: 255
    t.string   "lname",           limit: 255
    t.string   "email",           limit: 255
    t.string   "permission",      limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "password_digest", limit: 255
  end

  add_foreign_key "abet_metrics", "offerings"
  add_foreign_key "abet_outcomes", "tracks"
  add_foreign_key "airs", "documents"
  add_foreign_key "approvers", "tracks"
  add_foreign_key "approvers", "users"
  add_foreign_key "course_outcomes", "offerings"
  add_foreign_key "course_track_lists", "courses"
  add_foreign_key "course_track_lists", "tracks"
  add_foreign_key "eocms", "documents"
  add_foreign_key "libraries", "documents"
  add_foreign_key "libraries", "offerings"
  add_foreign_key "master_matrices", "abet_outcomes"
  add_foreign_key "master_matrices", "courses"
  add_foreign_key "master_matrices", "tracks"
  add_foreign_key "offerings", "courses"
  add_foreign_key "pies", "documents"
  add_foreign_key "teachings", "offerings"
  add_foreign_key "teachings", "users"
  add_foreign_key "tracks", "degrees"
end
