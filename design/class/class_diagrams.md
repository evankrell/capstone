## Class Diagrams

### Controllers

@startuml
class Authenticate {
   String CAS_URL
   verifyUser()
   connectToCAS()
}

class Manage_Users {
   Integer numUsers
   addUser()
   delUser()
   modUser()
   importUsers()
   exportUsers()
}

class Manage_Courses {
   integer numDegrees
   integer numTracks
   integer numCourses
   integer numOfferings
   addCourse()
   delCourse()
   modCourse()
   importCourses()
   exportCourses()
}

class Browse {
   integer numDocuments
   listTasks()
   showApproved()
   showUnreviewed()
   filter()
   search()
   bookmark()
   view()
   newDocument()
   export()
}

class Manage_Document {
   updateStatus()
   comment()
   deleteComment()
   readComments()
   link()
   unlink()
   showLinks()
   attachFile()
   showFiles()
   export()
}

class Manage_Form {
   autoPopulate()
   send()
   save()
}

class Manage_Master_Matrix {
   selectTrack()
   addOutcome()
   deleteOutcome()
   verify()
   save()
}

class Tracker {
   showTrackingSheets()
   showActionItems()
   filter()
   search()
}

@enduml
