@startuml
actor Admin
actor Evaluator
actor Instructor
actor Coordinator

interface Interface

boundary Login
boundary Config
boundary Library
boundary Master_Matrix
boundary Tracking
boundary Document
boundary Form

control Authenticate
control Manage_Users
control Manage_Courses
control Browse_Document
control Manage_Document
control Manage_Form
control Manage_Master_Matrix
control Tracker

entity CAS
entity User_Tables
entity Course_Tables
entity Document_Tables
entity Master_Matrix_Tables




Admin -- Interface
Instructor -- Interface
Coordinator -- Interface
Evaluator -- Interface
Interface -- Login
Interface -- Config
Interface -- Library
Interface -- Master_Matrix
Interface -- Tracking

Login -- Authenticate
Config -- Manage_Users
Config -- Manage_Courses
Library -- Browse_Document
Library -- Document
Document -- Form
Document -- Manage_Document
Master_Matrix -- Manage_Master_Matrix
Tracking -- Tracker
Form -- Manage_Form
Authenticate -- CAS
Authenticate -- User_Tables
Manage_Users -- User_Tables
Manage_Courses -- Course_Tables
Browse_Document -- Document_Tables
Manage_Master_Matrix -- Master_Matrix_Tables
Manage_Master_Matrix -- Course_Tables
Tracker -- Document_Tables
Manage_Form -- Document_Tables
Manage_Document -- Document_Tables


@enduml
